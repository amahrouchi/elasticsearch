# Explore your data

- Load d'un jeu de données random pour pouvoir faire des requetes plus réaliste.
- Les données sont fournies [ici](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started-explore-data.html)
- L'import se fait via la 1ere commande :
```
curl -H "Content-Type: application/json" -XPOST "localhost:9200/bank/_bulk?pretty&refresh" --data-binary "@accounts.json"
curl "localhost:9200/_cat/indices?v"
```

## The Search API

Version URI :
```
GET /bank/_search?q=*&sort=account_number:asc&pretty
```

Version JSON :
```
GET /bank/_search
{
  "query": { "match_all": {} },
  "sort": [
    { "account_number": "asc" }
  ]
}
```

Cette requête va chercher tous les documents de l'index `bank` en les classant de manière ascendante sur le champ `account_number`

Réponse partielle :
```json
{
  "took" : 63,
  "timed_out" : false,
  "_shards" : {
    "total" : 5,
    "successful" : 5,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
        "value": 1000,
        "relation": "eq"
    },
    "max_score" : null,
    "hits" : [ 
        {
          "_index" : "bank",
          "_type" : "_doc",
          "_id" : "0",
          "sort": [0],
          "_score" : null,
          "_source" : {"account_number":0,"balance":16623,"firstname":"Bradshaw","lastname":"Mckenzie","age":29,"gender":"F","address":"244 Columbus Place","employer":"Euron","email":"bradshawmckenzie@euron.com","city":"Hobucken","state":"CO"}
        }, {
          "_index" : "bank",
          "_type" : "_doc",
          "_id" : "1",
          "sort": [1],
          "_score" : null,
          "_source" : {"account_number":1,"balance":39225,"firstname":"Amber","lastname":"Duke","age":32,"gender":"M","address":"880 Holmes Lane","employer":"Pyrami","email":"amberduke@pyrami.com","city":"Brogan","state":"IL"}
        }
    ]
  }
}
```

Clefs les plus intéressantes dans la réponse :
- `_shards` : tracke le nombre de `shards` qui ont été utilisés pour cette recherche, le nombre qui ont répondus avec succès ou non. Ce champs peut être utile pour tracker le comportement de notre cluster et savoir quand il va mal.
- `hits.hits` : résultats de la recherche.
- `hits.total.value` : nombre total de résultat.
- `hits.total.relation` : indique si le total retourné est la valeur exacte ou une approximation (par défaut, des approximations sont fournies au-delà de 10K résultats). Si l'on souhaite tracker le total de manière précise, il faut setter le paramètre `track_total_hits=true` explicitement.

## Introducing the query language

Exemple de requête :
```
GET /bank/_search
{
  "query": { "match_all": {} },
  "from": 10,
  "size": 10,
  "sort": { "balance": { "order": "desc" } }
}
```

Cette requête cherche tous les documents de l'index `bank` ordonné par `balance` descendante et retourne les résultats 10 à 19.

- `from` : indice du 1er résultat à retourner (défaut : 0).
- `size` : nombre total de résultats à retourner (défault : 10).
- `sort` : configure la manière dont les résultats doivent être trier.

## Executing searches

- Sélection des champs à retourner dans le documents
```
GET /bank/_search
{
  "query": { "match_all": {} },
  "_source": ["account_number", "balance"]
}
```

Cette requête retourne tous les documents de l'index `bank` en restreignant les champs à afficher à ceux préciser dans `_source`. `_source` est l'équivalent de la clause `SELECT` en SQL.

### Quelques exemples de recherches simples sur un champ :

- Documents portant l'`account_number` 20 :
```
GET /bank/_search
{
  "query": { "match": { "account_number": 20 } }
}
```

- Documents dont l'adresse contient `mill` :
```
GET /bank/_search
{
  "query": { "match": { "address": "mill" } }
}
```

- Documents dont l'adresse contient `mill` ou `lane` :
```
GET /bank/_search
{
  "query": { "match": { "address": "mill lane" } }
}
```

- Documents dont l'adresse contient la phrase `mill lane` :
```
GET /bank/_search
{
  "query": { "match_phrase": { "address": "mill lane" } }
}
```

### Requêtes booléennes

- Documents contenant `mill` ET `lane` :
```
GET /bank/_search
{
  "query": {
    "bool": {
      "must": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}
```
`must` signifie que toutes les conditions précisées dans cette clause doivent être vraies pour que le document matche. 

- Documents contenant `mill` OU `lane` :
```
GET /bank/_search
{
  "query": {
    "bool": {
      "should": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}
```
`should` signifie qu'au moins une des conditions précisées dans cette clause doivent être vraie pour que le document matche. 

- Documents contenant NI `mill` NI `lane` :
```
GET /bank/_search
{
  "query": {
    "bool": {
      "must_not": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}
```
`must_not` signifie que toutes les conditions précisées dans cette clause doivent être fausses pour que le document matche.

- Documents de l'`age` est égal à `40` et dont le `state` est différent de `ID` :
```
GET /bank/_search
{
  "query": {
    "bool": {
      "must": [
        { "match": { "age": "40" } }
      ],
      "must_not": [
        { "match": { "state": "ID" } }
      ]
    }
  }
}
```

**Remarques :** On voit ici qu'on peut combiner les différents types de requêtes booléennes. On peut également les imbriquer de manière à créer la complexité nécessaire pour effectuer les requêtes voulues.

## Executing filters

Example de requête `bool` avec un filtre `range`:
```
GET /bank/_search
{
  "query": {
    "bool": {
      "must": { "match_all": {} },
      "filter": {
        "range": {
          "balance": {
            "gte": 20000,
            "lte": 30000
          }
        }
      }
    }
  }
}
```
Retournera tous les comptes dont la `balance` est comprise en 20ket 30k inclus.

**Remarques :** Dans le cas les scores ne sont pas calculés puisqu'inutiles. en effet, ils ne s'agit de simples filtres.

## Executing aggregations

- Comptes groupés par états, comptés et ordonnés par ce compte descendant :
```
GET /bank/_search
{
  "size": 0,
  "aggs": {
    "group_by_state": {
      "terms": {
        "field": "state.keyword"
      }
    }
  }
}
```
Équivalent SQL de :
```mysql
SELECT state, COUNT(*) FROM bank GROUP BY state ORDER BY COUNT(*) DESC LIMIT 10;
``` 

- Même requête avec le calcul de la `balance` moyenne par groupe et un tri descendant sur ce moyenne :
```
GET /bank/_search
{
  "size": 0,
  "aggs": {
    "group_by_state": {
      "terms": {
        "field": "state.keyword",
        "order": {
          "average_balance": "desc"
        }
      },
      "aggs": {
        "average_balance": {
          "avg": {
            "field": "balance"
          }
        }
      }
    }
  }
}
```
**Remarques :** Comme souvent dans les languages de requêtage de BDD, les aggrégations peuvent être imbriquées.

- Groupe les comptes par tranches d'`age`, puis par `gender` avec un calcul `balance` moyenne :
```
GET /bank/_search
{
  "size": 0,
  "aggs": {
    "group_by_age": {
      "range": {
        "field": "age",
        "ranges": [
          {
            "from": 20,
            "to": 30
          },
          {
            "from": 30,
            "to": 40
          },
          {
            "from": 40,
            "to": 50
          }
        ]
      },
      "aggs": {
        "group_by_gender": {
          "terms": {
            "field": "gender.keyword"
          },
          "aggs": {
            "average_balance": {
              "avg": {
                "field": "balance"
              }
            }
          }
        }
      }
    }
  }
}
```
**Remarques :** Le compte est donné pour chacun des niveaux d'aggrégation. Pratique !
