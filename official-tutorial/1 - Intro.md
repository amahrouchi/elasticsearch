##Prérequis

- Source : https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html
- Système :
```
sudo sysctl -w vm.max_map_count=262144
```
Sans cette commande, la mémoire virtuelle nécessaire pour faire tourner ES dans un docker n'est pas suffisante.

## Exploring your cluster

### Create an index

- Creation d'un index `customer` puis affichage de la liste des index du serveur

```
PUT /customer?pretty
GET /_cat/indices?v
```

Le nouvel `index` créé a un statut `yellow` car par défaut ES lui associe un `replica` qui ne peut être à aucun `node`.
En effet, notre architecture ES ne possédant qu'un seul `node`, le replica ne peut pas être actif car il faut qu'il soit
sur un node different du `shard` principal.


### Index and query a document

- Pour créer un nouveau document dans l'index `customer` en spécifiant son ID:
```
PUT /customer/_doc/1?pretty
{
    "name" : "John Doe"
}
```

- Pour créer un nouveau document dans l'index `customer` avec un ID auto-généré :
```
POST /customer/_doc?pretty
{
    "name" : "John Doe"
}
```

- Récupérer un document via son ID :
```
GET /customer/_doc/1?pretty
```

- Voir le contenu total de l'index `customer` :
```
POST /customer/_search?pretty
{
    "query" : {
        "match_all" : {}
    }
}
```

### Delete an index

- Effacer l'index `customer` puis regarder la liste des index :
```
DELETE /customer?pretty
GET /_cat/indices?v
```

- Remarques concernant la construction des URLs de l'API REST :
```
<HTTP Verb> /<index>/<endpoint>/<id>
```

Toutes les URLs de l'API sont construites sur ce modèle, le retenir est un chose importante !

## Modifying your data

### Updating documents

- Modifier le `name` et ajouter un champ `age` :
```
POST /customer/_update/1?pretty
{
    "doc": { "name": "Jane Doe", "age" : 20 }
}
```
**Remarque :** les autres champs de ce document ne seront pas modifiés ou supprimés par cet appel.

- Modifier un document dans sa totalité:
```
PUT /customer/_update/1?pretty
{
    {"name": "John Doe", "sex" : "m"}
}
```
**Remarque :** le document est totalement écrasé par les nouvelles données fournies.

### Deleting documents

- Supprimer un document via son ID :
```
DELETE /customer/_doc/2?pretty
```

- **Remarque :** Il existe aussi la possibilité de supprimer des documents par recherche en utilisant lAPI `_delete_by_query`

### Batch processing

- Faire des operations CRUD en batch :
```
POST /customer/_bulk?pretty
{"index":{"_id":"1"}}
{"name": "John Doe" }
{"index":{"_id":"2"}}
{"name": "Jane Doe" }
{"update":{"_id":"3"}}
{"doc": { "name": "John Doe becomes Jane Doe" } }
{"delete":{"_id":"4"}}
```
Ici, les 2 premières instructions permettent d'indexer 2 nouveaux documents. La 3ème instruction met à jour le champ `name` documents portant l'id 3. La 4eme instruction efface le document portant l'id 4.
