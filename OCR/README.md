## Source

- [Tutoriel OpenClassRoom](https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4474691-etudiez-le-fonctionnement-d-elasticsearch)

## Etudier le fonctionnement d'ES

### Importer les données

Il y a 2 manieres de créer un nouvel index :

- Importer directement le fichier de données :
```
curl -XPUT -H "Content-Type: application/json" localhost:9200/_bulk --data-binary @mapping_movies/movies_elastic2.json
```
Dans ce cas, le mapping est généré automatiquement par ES ce qui peut parfois poser des problemes car aucun type `raw` ne sera créé.

- Commencer par importer le fichiers de mapping, puis ensuite importer les données:
```
curl -XPUT -H "Content-Type: application/json" localhost:9200/movies2 -d @mapping_movies/mapping.json
```
```
curl -XPUT -H "Content-Type: application/json" localhost:9200/_bulk --data-binary @mapping_movies/movies_elastic2.json
```
**Remarque :** le mapping ne peut pas etrer changer une fois l'index créé, il faut donc le supprimer, importer le mapping puis importer les données.

### Visualiser les données

Pour visualiser le mapping de l'index désiré, il faut appeler l'URL :
```
http://localhost:9200/movies/?pretty
```

Pour visualiser la totalité des données présente dans un index, il faut appeler l'URL :
```
http://localhost:9200/[INDEX]/[TYPE]/_search
http://localhost:9200/movies/movie/_search
```
**Remarque :** chaque entrée du JSON d'import doit préciser l'index et le type des données. C'est donc pour ça que nous retrouvons ces précisions dans l'URL de recherche.

### Remarques
- Le second mapping précise que certains champs sont des données brutes `raw`. Cela signifie que le contenu ne doit pas être découpé en mot clef mais doit être considéré comme une unité complète. Ça permettra notamment de grouper les résultats sur ces valeurs dans les chapitres suivants.